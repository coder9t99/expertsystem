//
//  ExpertSystemRepoIntegrationTests.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import UIKit
import XCTest
import ExpertSystem

class ExpertSystemRepoIntegrationTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testIntegrationRepoScenarios(){
        // arrange
        let expectation = expectationWithDescription("GET testIntegrationRepoScenarios()")
        let repo = ExpertSystemRepo(connector: HttpRestfulServiceConnector(session: NSURLSession.sharedSession()), defaultErrorHandler:nil)
        
        // action
        repo.scenarios(nil) { (result) in
            let scenariosResult = result
            
            // assert
            XCTAssertEqual(scenariosResult.scenariosArray.count, 2, "did not parse scenarios correctly")
            XCTAssertEqual(scenariosResult.scenariosArray[0].id, 1,
                "did not parse property 'id' on first scenario correctly")
            XCTAssertEqual(scenariosResult.scenariosArray[0].text, "Problems with DSL connection",
                "did not parse property 'text' on first scenario correctly")
            XCTAssertEqual(scenariosResult.scenariosArray[0].caseId, 1,
                "did not parse property 'caseId' on first scenario correctly")
            XCTAssertEqual(scenariosResult.scenariosArray[1].id, 2,
                "did not parse property 'id' on second scenario correctly")
            XCTAssertEqual(scenariosResult.scenariosArray[1].text, "Printer troubles",
                "did not parse property 'text' on second scenario correctly")
            XCTAssertEqual(scenariosResult.scenariosArray[1].caseId, 8,
                "did not parse property 'caseId' on second scenario correctly")
            
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(20, handler: nil)
    }
    
    func testIntegrationRepoCase(){
        // arrange
        let expectation = expectationWithDescription("GET testIntegrationRepoScenarios()")
        let repo = ExpertSystemRepo(connector: HttpRestfulServiceConnector(session: NSURLSession.sharedSession()), defaultErrorHandler:nil)
        
        // action
        repo.cases(1, failed: nil) { (result) in
            let caseResult = result
            
            // assert
            //   case
            XCTAssertEqual(caseResult.`case`.id, 1,
                "did not parse property 'id' correctly")
            XCTAssertEqual(caseResult.`case`.text, "Is your phone cable connected?",
                "did not parse property 'text' correctly")
            XCTAssertEqual(caseResult.`case`.image!, "https://s3-ap-southeast-1.amazonaws.com/ns-expert-system/phone_cable.jpg",
                "did not parse property 'image' correctly")
            XCTAssertEqual(caseResult.`case`.answersArray.count, 2,
                "did not parse property 'answers' correctly")
            
            //   case.answers[0]
            XCTAssertEqual(caseResult.`case`.answersArray[0].id, 1,
                "did not parse property 'id' correctly")
            XCTAssertEqual(caseResult.`case`.answersArray[0].caseId, 3,
                "did not parse property 'text' correctly")
            XCTAssertEqual(caseResult.`case`.answersArray[0].text, "Yes",
                "did not parse property 'image' correctly")
            
            //   case.answers[1]
            XCTAssertEqual(caseResult.`case`.answersArray[1].id, 2,
                "did not parse property 'id' correctly")
            XCTAssertEqual(caseResult.`case`.answersArray[1].caseId, 2,
                "did not parse property 'text' correctly")
            XCTAssertEqual(caseResult.`case`.answersArray[1].text, "No",
                "did not parse property 'image' correctly")
            
            expectation.fulfill()
        }
        waitForExpectationsWithTimeout(20, handler: nil)
    }

}
