//
//  KeyValueModelTests.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import UIKit
import XCTest
import ExpertSystem

class KeyValueModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testScenariosResultPerformance() {
        let jsonString = "{\"scenarios\": [ {\"text\": \"Problems with DSL connection\", \"id\": 1, \"caseId\": 1}, {\"text\": \"Printer troubles\", \"id\": 2, \"caseId\": 8}]}"
        let jsonStringData = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        self.measureBlock() {
            let scenariosResult = ScenariosResult(jsonStringData: jsonStringData!)
        }
    }
    
    func testCaseResultPerformance() {
        let jsonString = "{\"case\": {\"text\": \"Is your phone cable connected?\", \"image\": \"https://s3-ap-southeast-1.amazonaws.com/ns-expert-system/phone_cable.jpg\", \"id\": 1, \"answers\": [{\"text\": \"Yes\", \"id\": 1, \"caseId\": 3}, {\"text\": \"No\", \"id\": 2, \"caseId\": 2}]}}"
        let jsonStringData = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        self.measureBlock() {
            let caseResult = CaseResult(jsonStringData: jsonStringData!)
        }
    }
    
    func testScenariosResultParsing(){
        // arrange
        let jsonString = "{\"scenarios\": [ {\"text\": \"Problems with DSL connection\", \"id\": 1, \"caseId\": 1}, {\"text\": \"Printer troubles\", \"id\": 2, \"caseId\": 8}]}"
        let jsonStringData = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        // action
        let scenariosResult = ScenariosResult(jsonStringData: jsonStringData!)
        
        // assert
        XCTAssertEqual(scenariosResult.scenariosArray.count, 2, "did not parse scenarios correctly")
        XCTAssertEqual(scenariosResult.scenariosArray[0].id, 1,
            "did not parse property 'id' on first scenario correctly")
        XCTAssertEqual(scenariosResult.scenariosArray[0].text, "Problems with DSL connection",
            "did not parse property 'text' on first scenario correctly")
        XCTAssertEqual(scenariosResult.scenariosArray[0].caseId, 1,
            "did not parse property 'caseId' on first scenario correctly")
        XCTAssertEqual(scenariosResult.scenariosArray[1].id, 2,
            "did not parse property 'id' on second scenario correctly")
        XCTAssertEqual(scenariosResult.scenariosArray[1].text, "Printer troubles",
            "did not parse property 'text' on second scenario correctly")
        XCTAssertEqual(scenariosResult.scenariosArray[1].caseId, 8,
            "did not parse property 'caseId' on second scenario correctly")
    }
    
    func testCaseResultParsing(){
        // arrange
        let jsonString = "{\"case\": {\"text\": \"Is your phone cable connected?\", \"image\": \"https://s3-ap-southeast-1.amazonaws.com/ns-expert-system/phone_cable.jpg\", \"id\": 1, \"answers\": [{\"text\": \"Yes\", \"id\": 1, \"caseId\": 3}, {\"text\": \"No\", \"id\": 2, \"caseId\": 2}]}}"
        let jsonStringData = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        
        // action
        let caseResult = CaseResult(jsonStringData: jsonStringData!)
        
        // assert
        //   case
        XCTAssertEqual(caseResult.`case`.id, 1,
            "did not parse property 'id' correctly")
        XCTAssertEqual(caseResult.`case`.text, "Is your phone cable connected?",
            "did not parse property 'text' correctly")
        XCTAssertEqual(caseResult.`case`.image!, "https://s3-ap-southeast-1.amazonaws.com/ns-expert-system/phone_cable.jpg",
            "did not parse property 'image' correctly")
        XCTAssertEqual(caseResult.`case`.answersArray.count, 2,
            "did not parse property 'answers' correctly")
        
        //   case.answers[0]
        XCTAssertEqual(caseResult.`case`.answersArray[0].id, 1,
            "did not parse property 'id' correctly")
        XCTAssertEqual(caseResult.`case`.answersArray[0].caseId, 3,
            "did not parse property 'text' correctly")
        XCTAssertEqual(caseResult.`case`.answersArray[0].text, "Yes",
            "did not parse property 'image' correctly")
        
        //   case.answers[1]
        XCTAssertEqual(caseResult.`case`.answersArray[1].id, 2,
            "did not parse property 'id' correctly")
        XCTAssertEqual(caseResult.`case`.answersArray[1].caseId, 2,
            "did not parse property 'text' correctly")
        XCTAssertEqual(caseResult.`case`.answersArray[1].text, "No",
            "did not parse property 'image' correctly")
    }
}
