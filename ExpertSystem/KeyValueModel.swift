//
//  KeyValueModel.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import Foundation

public class KeyValueModel:NSObject, KeyValueModelProtocol {
    // MARK - Static members
    /** static container */
    private struct Static {
        static var modelBuilder:KeyValueModelBuilderProtocol?
        static var dispatchToken: dispatch_once_t = 0
    }
    
    /** static property to access ModelBuilder */
    class var ModelBuilder:KeyValueModelBuilderProtocol {
        /** self instantiate as SwiftJSONKeyValueModelBuilder if not wired */
        get {
            if let builder = Static.modelBuilder {
                return builder
            } else {
                let builder = SwiftJSONKeyValueModelBuilder() // <== instantiate
                KeyValueModel.ModelBuilder = builder
                return builder
            }
        }
        set {
            dispatch_once(&Static.dispatchToken) { Static.modelBuilder = newValue }
        }
    }
    
    // MARK : - c'tors
    
    public override required init(){ super.init()}
    
    public required init(jsonStringData:NSData) {
        super.init()
        KeyValueModel.ModelBuilder.build(jsonStringData, model:self)
    }
    
    public required init(dictionary:Dictionary<String, AnyObject>) {
        super.init()
        let this = self
        dictionary
            .filter{ this.respondsToSelector(NSSelectorFromString($0.0 as String)) }
            .apply { this.setValue($0.1, forKey:$0.0 as String)                    }
    }
    
    /** reflects model properties names */
    public func propertyNames() -> [String] {
        var propertyNames: [String] = []
        var count: UInt32 = 0
        var properties = class_copyPropertyList(classForCoder, &count)
        for i in 0..<Int(count) {
            propertyNames.append(
                String.fromCString(
                    property_getName(properties[i]))!)
        }
        free(properties)
        return propertyNames
    }
    
    public func classForArrayProperty(propertyName:String) -> Any.Type? {
        return KeyValueModel.self
    }
}
