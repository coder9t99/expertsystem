//
//  HttpRestfulServiceConnector.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** service connector for http restful services */
public class HttpRestfulServiceConnector : ServiceConnectorProtocol {
    
    public init(session:NSURLSession) {
        self.session = session
    }
    
    public func execute<T:KeyValueModelProtocol>(endpoint:String, failed:(NSError -> Void)?, success:T -> Void) {
        let url = NSURL(string:endpoint)!
        let task = self.session.dataTaskWithURL(url, completionHandler: { (data, response, error) in
            if let errorValue = error {
                self.handleError(errorValue, failed:failed)
            } else {
                println(NSString(data:data, encoding: NSUTF8StringEncoding)!)
                success(T(jsonStringData: data))
            }
        })
        
        task.resume()
        
    }
    
    /** shorthand for error handling. prints to error log if failed callback not supplied */
    private func handleError(error:NSError, failed:(NSError -> Void)?) {
        if let failedCallback = failed {
            failedCallback(error)
            return
        }
        println(error)
    }
    
    private var session:NSURLSession
}
