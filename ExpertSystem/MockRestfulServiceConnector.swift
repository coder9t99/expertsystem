//
//  MockRestfulServiceConnector.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import ExpertSystem

public class MockRestfulServiceConnector : ServiceConnectorProtocol {
    public func execute<T:KeyValueModelProtocol>(endpoint:String, failed:(NSError -> Void)?, success:T -> Void) {
        switch endpoint {
        case "http://expert-system.internal.shinyshark.com/scenarios/":
            handleScenariosResult(success)
        case let ep where ep.hasPrefix("http://expert-system.internal.shinyshark.com/cases/"):
            handleCaseResult(success)
        default:
            let error = NSError(domain: "MockRestfulServiceConnector", code: 404, userInfo: nil)
            self.handleError(error, failed: failed)
        }
    }
    public init(){}
    
    func handleError(error:NSError, failed:(NSError -> Void)?) {
        if let failedCallback = failed {
            failedCallback(error)
            return
        }
        
        println(error)
    }
    
    func handle<T:KeyValueModelProtocol>(jsonString:String, success:T -> Void){
        let jsonStringData = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        success(T(jsonStringData: jsonStringData!))
    }
    
    func handleScenariosResult<T:KeyValueModelProtocol>(success:T -> Void) {
        handle("{\"scenarios\": [ {\"text\": \"Problems with DSL connection\", \"id\": 1, \"caseId\": 1}, {\"text\": \"Printer troubles\", \"id\": 2, \"caseId\": 8}]}", success:success)
    }
    
    
    func handleCaseResult<T:KeyValueModelProtocol>(success:T -> Void) {
        handle("{\"case\": {\"text\": \"Is your phone cable connected?\", \"image\": \"https://s3-ap-southeast-1.amazonaws.com/ns-expert-system/phone_cable.jpg\", \"id\": 1, \"answers\": [{\"text\": \"Yes\", \"id\": 1, \"caseId\": 3}, {\"text\": \"No\", \"id\": 2, \"caseId\": 2}]}}", success:success)
    }
}