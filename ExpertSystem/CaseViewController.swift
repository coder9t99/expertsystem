//
//  CaseViewController.swift
//  ExpertSystem
//
//  Created by Q on 8/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import UIKit

class CaseViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: - Interface Builder Outlets
    @IBOutlet weak var label : UILabel!
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var tableView : UITableView!
    
    // MARK: - Constants
    let CellIdentifier:String = "AnswersCellIdentifier";
    
    // MARK: - Properties
    
    /** caseId - when set, invokes data loading */
    var caseId : Int? {
        didSet { loadData() }
    }
    
    /** data - DataSource for Answers TableView */
    var data:CaseResult?
    
    
    // MARK: - Data Loading
    func loadData() {
        if let id = self.caseId {
            expertSystemRepo.cases(id, nil) {
                self.data = $0
                self.refreshView()
            }
        } else {
            self.data = nil
            self.refreshView()
        }
        
    }
    
    func loadImageAsync(imageUrl:String) {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)) {
            let url = NSURL(string:imageUrl)!
            let req = NSURLRequest(URL: url)
            
            NSURLConnection.sendAsynchronousRequest(req, queue: NSOperationQueue.mainQueue()) {
                (res, data, error) in
                if let errorValue = error {
                    if let handler = self.defaultErrorHandler {
                        handler.handle(error)
                    }
                } else {
                    self.imageView.image = UIImage(data: data!)
                }
            }
        }
    }
    
    func refreshView() {
        dispatch_async(dispatch_get_main_queue()) {
            self.title = "Case \(self.caseId!)"
            self.label.text = ""
            if let theCase = self.data?.`case` {
                self.label.text = "\(theCase.text)"
                if let image = theCase.image {
                    self.loadImageAsync(image)
                }
            }
            
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.data == nil ? 0 : 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let d = self.data {
            return d.`case`.answersArray.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as UITableViewCell
        
        if let item = self.data?.`case`.answersArray[indexPath.row] {
            cell.textLabel?.text = item.text
            cell.detailTextLabel?.text = "case:\(item.caseId) id:\(item.id)"
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let caseViewController: CaseViewController =
            self.storyboard?
                .instantiateViewControllerWithIdentifier("CaseViewController") as? CaseViewController {
                    
            if let item = self.data?.`case`.answersArray[indexPath.row] {
                caseViewController.caseId = item.caseId
            }
                    
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
                    
            if let navCtrl = appDelegate.window?.rootViewController as? UINavigationController {
                navCtrl.pushViewController(caseViewController, animated: true)
            }
        }
    }
}
