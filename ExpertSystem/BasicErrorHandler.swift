//
//  BasicErrorHandler.swift
//  ExpertSystem
//
//  Created by Q on 8/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import UIKit

class BasicErrorHandler:ErrorHandlerProtocol {
    func handle(error:NSError) {
        var alertView = UIAlertView();
        alertView.addButtonWithTitle("Ok");
        alertView.title = "Snap!!";
        alertView.message = "Looks like there were something wrong :(";
        alertView.show();
    }
}