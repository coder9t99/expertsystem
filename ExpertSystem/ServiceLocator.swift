//
//  ServiceLocator.swift
//  ExpertSystem
//
//  Created by Q on 8/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import Foundation
import UIKit

class ServiceLocator {
    init(defaultErrorHandler:ErrorHandlerProtocol?){
        self.defaultErrorHandler = defaultErrorHandler
    }
    private var _expertSystemRepo : ExpertSystemRepo?
    var defaultErrorHandler:ErrorHandlerProtocol?
    var expertSystemRepo:ExpertSystemRepo {
        
        return ExpertSystemRepo(
        connector: HttpRestfulServiceConnector(session: NSURLSession.sharedSession()),
        defaultErrorHandler: self.defaultErrorHandler)
    }
}

extension UIViewController {
    var appDelegate : AppDelegate {
        get {
            return UIApplication.sharedApplication().delegate as AppDelegate
        }
    }
    
    var expertSystemRepo : ExpertSystemRepo {
        get {
            return appDelegate.serviceLocator.expertSystemRepo
        }
    }
    
    var defaultErrorHandler:ErrorHandlerProtocol? {
        get {
            return appDelegate.serviceLocator.defaultErrorHandler
        }
    }
    
    var serviceLocator : ServiceLocator { get {
            let appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
            return appDelegate.serviceLocator
        }
    }
}