//
//  DictionaryExtensions.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

extension Dictionary {
    init(_ pairs: [Element]) {
        self.init()
        for (k, v) in pairs {
            self[k] = v
        }
    }
    
    func map<TKey: Hashable, TValue>(transform: Element -> (TKey, TValue)) -> [TKey: TValue] {
        return Dictionary<TKey, TValue>(Swift.map(self, transform))
    }
    
    func filter(includeElement: Element -> Bool) -> [Key: Value] {
        return Dictionary(Swift.filter(self, includeElement))
    }
    
    func apply(apply: (Dictionary.Generator.Element) -> Void){
        for (k, v) in self {
            apply(k, v)
        }
    }
}