//
//  KeyValueModelBuilderProtocol.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** model builder protocol */
public protocol KeyValueModelBuilderProtocol
{
    /** populate JSON data into instantiated model object */
    func build<T where T:NSObject, T:KeyValueModelProtocol>(jsonStringData:NSData, model:T)
}