//
//  ArrayExtension.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

extension Array{
    func apply(apply:(T) -> ()){
        for (idx, element) in enumerate(self) { apply(element) }
    }
}

