//
//  KeyValueModelProtocol.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** KeyValue Model protocol */
public protocol KeyValueModelProtocol
{
    init(dictionary:Dictionary<String, AnyObject>)
    
    /**
     * ## for this excerise, jsonString was the only data input
     *    therefore I made model class base to instantiate by jsonString
     *    which might not nessacery and can be consider for further decoupling
     */
    init(jsonStringData:NSData)
    
    /**
     * ## as part of model building process
     *    property name is required to reduce
     *    resource usage
     */
    func propertyNames() -> [String]
    
    /**
     * for any ArrayProperty to be deserialised subclass must
     * respond to this method to inform model builder the element type of the array
     * 
     * ## it is possible to get model builder to resolve array element type.
     */
    func classForArrayProperty(propertyName:String) -> Any.Type?
}