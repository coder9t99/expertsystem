//
//  ScenariosTableViewController.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import UIKit

class ScenariosTableViewController: UITableViewController {
    
    // MARK: - Constants
    let CellIdentifier:String = "ScenariosCellIdentifier";

    // MARK: - properties
    /** DataSource for Scenarios TableView */
    var data:ScenariosResult?
    
    // MARK: - View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()

        expertSystemRepo.scenarios(nil) {
            self.data = $0
            dispatch_async(dispatch_get_main_queue()) { self.tableView.reloadData() }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.data == nil ? 0 : 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let scenarioResult = self.data {
            return scenarioResult.scenariosArray.count
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier) as UITableViewCell
        
        if let item = self.data?.scenariosArray[indexPath.row] {
            cell.textLabel?.text = item.text
            cell.detailTextLabel?.text = "case:\(item.caseId) id:\(item.id)"
        }
        return cell
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let caseViewController = segue.destinationViewController as CaseViewController
        
        if let caseId = self.data?.scenariosArray[self.tableView.indexPathForSelectedRow()!.row].caseId {
            caseViewController.caseId = caseId
        }
    }

}
