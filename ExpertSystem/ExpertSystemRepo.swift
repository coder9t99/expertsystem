//
//  ExpertSystemRepo.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** ExpertSystem Repository */
public class ExpertSystemRepo {
    
    /**
     * connector : service connector, used to connect to end points
     * defaultErrorHandler : default error handling
     */
    public init(connector:ServiceConnectorProtocol, defaultErrorHandler:ErrorHandlerProtocol?) {
        self.connector = connector
        errorHandler = defaultErrorHandler
    }
    
    /**
     * executeWithEndpoint:failed:success
     * execute endpoint.
     * invokes 'success' callback with result model object
     *
     * error is handled by invokes 'failed' handler callback.
     * OR invokes defaultErrorHandler as secondary handler
     */
    func execute<T:KeyValueModelProtocol>(endpoint:String, failed:(NSError -> Void)?, success:T -> Void) {
        let failedHandler:(NSError -> Void)? = (failed == nil) ? self.defaultErrorHandler : failed
        connector.execute(endpoint, failed: failedHandler, success: success)
    }
    
    /**
     * scenarios
     * executes scenarios endpoint
     * invokes 'success' callback with parsed ScenariosResult object
     *
     * error is handled by invokes 'failed' handler callback.
     * OR invokes defaultErrorHandler as secondary handler
     */
    public func scenarios(failed:(NSError -> Void)?, success:ScenariosResult -> Void) {
        // TODO : Consider move following endpoint to PLIST?
        let endpoint = "http://expert-system.internal.shinyshark.com/scenarios/"
        self.execute(endpoint, failed: failed, success: success)
    }
    
    /**
     * cases
     * executes cases endpoint with caseId
     *
     * invokes 'success' callback with parsed CaseResult object
     *
     * error is handled by invokes 'failed' handler callback.
     * OR invokes defaultErrorHandler as secondary handler
     */
    public func cases(caseId:Int, failed:(NSError -> Void)?, success:CaseResult -> Void) {
        // TODO : Consider move following endpoint to PLIST?
        let endpoint = "http://expert-system.internal.shinyshark.com/cases/\(caseId)"
        self.execute(endpoint, failed: failed, success: success)
    }
    
    /**
     * callback for default error handling.
     * this is made as lazy loaded property so that
     * it is prepared only when needed.
     */
    lazy var defaultErrorHandler:(NSError -> Void)? = { error in
        if let handler = self.errorHandler {
            handler.handle(error)
        }
    }
    
    private var connector:ServiceConnectorProtocol
    private var errorHandler:ErrorHandlerProtocol?
}