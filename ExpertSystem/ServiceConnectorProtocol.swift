//
//  ServiceConnectorProtocol.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** service connector, used to connect to end points */
public protocol ServiceConnectorProtocol {
    /**
     * execute
     * execute endpoint.
     * invokes 'success' callback with result model object
     *
     * error is handled by invokes 'failed' handler callback.
     * OR invokes defaultErrorHandler as secondary handler
     */
    func execute<T:KeyValueModelProtocol>(endpoint:String, failed:(NSError -> Void)?, success:T -> Void)
}