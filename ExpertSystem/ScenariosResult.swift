//
//  ScenariosResult.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** model class for ScenariosResult - root element of /scenarios/ endpoint */
public class ScenariosResult : KeyValueModel
{
    public var scenarios : NSMutableArray = []
    
    public var scenariosArray : Array<Scenario> { return scenarios as AnyObject as Array<Scenario> }
    
    public override func classForArrayProperty(propertyName:String) -> Any.Type? {
        if propertyName == "scenarios" { return Scenario.self }
        return nil
    }
}