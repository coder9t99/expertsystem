//
//  Case.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** model class for case */
public class Case : KeyValueModel
{
    public var id    : Int    = 0
    public var image : String? = ""
    public var text  : String = ""
    public var answers : NSMutableArray = []
    
    public var answersArray : Array<Answer> { return answers as AnyObject as Array<Answer> }
    
    public override func classForArrayProperty(propertyName:String) -> Any.Type? {
        if propertyName == "answers" { return Answer.self }
        return nil
    }
}
