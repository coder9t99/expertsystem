//
//  Scenario.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** model class for Scenario */
public class Scenario : KeyValueModel
{
    public var id     : Int    = 0
    public var text   : String = ""
    public var caseId : Int    = 0
}