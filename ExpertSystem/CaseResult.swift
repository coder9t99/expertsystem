//
//  CaseResult.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

/** model class for CaseResult - root element of /cases/ endpoint */
public class CaseResult : KeyValueModel
{
    public var `case` : Case = Case()
}