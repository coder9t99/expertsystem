//
//  ErrorHandlerProtocol.swift
//  ExpertSystem
//
//  Created by Q on 8/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

public protocol ErrorHandlerProtocol {
    func handle(error:NSError)
}
