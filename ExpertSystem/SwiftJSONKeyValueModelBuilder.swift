//
//  SwiftJSONKeyValueModelBuilder.swift
//  ExpertSystem
//
//  Created by Q on 7/12/2014.
//  Copyright (c) 2014 Q. All rights reserved.
//

import Foundation

/** default kv model builder */
public class SwiftJSONKeyValueModelBuilder : KeyValueModelBuilderProtocol
{
    /**
     * kick starts model building process
     */
    public func build<T where T:NSObject, T:KeyValueModelProtocol>(jsonStringData:NSData, model:T) {
        let json = JSON(data:jsonStringData)
        build(json, model:model)
    }
    
    /**
     * entry point for SwiftyJSON'd object.
     * this build function is designed for recursive calls 
     * so that nested models can be parsed correclty
     *  e.g. case.answers[]
     */
    private func build<T where T:NSObject, T:KeyValueModelProtocol>(json:JSON, model:T) {
        if json.type != .Dictionary {return}
        
        json.dictionaryValue
            .filter{ model.respondsToSelector(NSSelectorFromString($0.0 as String)) }
            .apply { model.setValue(self.resolveJSONValue($0.1, onModel:model, forKey:$0.0 as String), forKey:$0.0 as String) }
    }
    
    /**
     * resovles property value
     * ## initially was thinking to do strategy pattern with this function,
     *    was taking a while to work out generalisation of each different
     *    property type. especially on the Swift.Array<T> property types
     *    due to time constraint, I have left this function using switch
     *    for conditional flow.
     *    this function is a good candidate for improvement.
     */
    private func resolveJSONValue<T where T:NSObject, T:KeyValueModelProtocol>(json:JSON, onModel:T, forKey:String) -> AnyObject? {
        var dict : [String:Any.Type] = { (model:NSObject) -> [String:Any.Type] in
            var retval = [String:Any.Type] ()
            let mirror = reflect(model)
            for i in 0..<mirror.count {
                let (key, mirrorType) = mirror[i]
                retval[key] = mirrorType.valueType
            }
            return retval
            }(onModel)
        switch json.type {
        case .Number: return json.numberValue
        case .String : return json.stringValue
        case .Bool : return json.boolValue
        case .Array:
            var nsArray:NSMutableArray = NSMutableArray()
            if let klass = onModel.classForArrayProperty(forKey) as? KeyValueModel.Type {
                for innerJson : JSON in json.arrayValue {
                    let className = ReflectUtil.getClassName(klass())
                    nsArray.addObject(resolveDictionaryResult(innerJson, targetTypeName:className)!)
                }
                return nsArray
            }
            return nil
        case .Dictionary :
            if let klass = dict[forKey] as? KeyValueModel.Type {
                let className = ReflectUtil.getClassName(klass())
                return resolveDictionaryResult(json, targetTypeName:className)
            }
            return nil
            
        case .Null : return nil
        case .Unknown : return nil
        }
    }
    
    /**
     * helper for dictioanry type / nested model
     * ## this function utilises swift-factory to instantiate nested model
     *    takes the instantiated model and feed it into json entry build function
     *    recursively
     */
    private func resolveDictionaryResult(json:JSON, targetTypeName:String) -> KeyValueModel? {
        if let obj = ObjectFactory<KeyValueModel>.createInstance(className: targetTypeName) {
            build(json, model:obj)
            return obj
        } else {
            return nil
        }
    }
}