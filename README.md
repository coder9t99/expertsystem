# README #

This piece of work is done in response to an ExpertSystem test to consume following web service

* Root URL: [http://expertsystem.internal.shinyshark.com/](http://expertsystem.internal.shinyshark.com/)

* Format: JSON (application/json)

* Endpoints:
    * [scenarios/](http://expertsystem.internal.shinyshark.com/scenarios/) list of scenarios
    * [cases/](http://expertsystem.internal.shinyshark.com/cases/) :id  case details


### Quick summary ###
The work was done in **Swift** language with intention to demonstrate my ability on adapting new technology as well as capability utilising iOS UIKit framework.

### Assumptions ###
endpoints support HTTP GET request

### Minimum Device Requirement ###
OS version : iOS 7 (swift min version)
Devices:

* iPhone 4+
* iPad 2+, Air, mini
* iPod Touch 5th Gen +

### 3rd Party Libraries ###

* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) - parse and safe check JSON response
* [swift-factory](https://github.com/ijoshsmith/swift-factory) - instantiating model objects


### Design Patterns ###
Repository, Service Locator and Builder Pattern



### How do I get set up? ###

* Set up - clone or download, open ExpertSystem.xcodeproj with Xcode 6 +

* Run tests - Cmd + U

* RUN - Cmd + R



### TODOs / To Be Improved ###

* UX / UI:
    The work was focusing on functionality, architectural design, and coding style/capability.
    However for commercial, I’ve done differently to UX/UI
    as I believe without good look, app just won’t shine.


* Unit Test:
    due to time constraint, UnitTest wasn’t testing each decoupled classes.
    Instead each test targets multiple classes through layers.
    Intention was to test most important functionality.


* Dependency Injection:
    Dependency Injection container was not used. instead a quick service locator was written for basic dependency resolution. This should definitely be improved if time allows


* Add Business Logic Layer:
     since the requirement was quite short/small, repository layer was directly accessed by presentation layer


* Lazy fill to Improving Model building performance : the model building performance is high. However lazy filling with Proxy pattern may provide further speed boost.


* Continuous Integration:
     um... probably overkill for such small piece of work....





### Detail Design Summary ###
To Be Added upon request...